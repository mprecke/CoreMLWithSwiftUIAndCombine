//
//  CoreMLWithSwiftUIAndCombineApp.swift
//  CoreMLWithSwiftUIAndCombine
//
//  Created by Moritz Philip Recke on 11.03.21.
//

import SwiftUI

@main
struct CoreMLWithSwiftUIAndCombineApp: App {
    var body: some Scene {
        WindowGroup {
            ImageClassificationView()
        }
    }
}
