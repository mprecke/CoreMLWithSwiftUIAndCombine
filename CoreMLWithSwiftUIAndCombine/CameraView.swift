//
//  CameraView.swift
//  CoreMLWithSwiftUIAndCombine
//
//  Created by Moritz Philip Recke on 11.03.21.
//

import SwiftUI

struct CameraView: View {
    
    @Binding var showImagePicker: Bool
    @Binding var image: Image?
    
    var body: some View {
        ImagePicker(isShown: $showImagePicker, image: $image, sourceType: .photoLibrary)
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CameraView(showImagePicker: .constant(false), image: .constant(Image("lemon")))
        }
    }
}
