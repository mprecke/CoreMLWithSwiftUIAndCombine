//
//  ImageClassificationViewModel.swift
//  CoreMLWithSwiftUIAndCombine
//
//  Created by Moritz Philip Recke on 11.03.21.
//

import Foundation
import SwiftUI
import Combine

class ImageClassificationViewModel: ObservableObject {
    
    var name: String = ""
    var manager: ImageClassificationManager
    
    @Published var predictionLabel: String = ""
    
    init(manager: ImageClassificationManager) {
        self.manager = manager
    }
    
    func detect(_ name: String) {
        
        let sourceImage = UIImage(named: name)
        
        guard let resizedImage = sourceImage?.resizeImageTo(size: CGSize(width: 224, height: 224)) else {
            fatalError("Unable to resize the image!")
        }
        
        if let label = self.manager.detect(resizedImage) {
            self.predictionLabel = label
        }
      
    }
    
    func detect(image: Image) {
        
        let sourceImage: UIImage = image.asUIImage()
        
        guard let resizedImage = sourceImage.resizeImageTo(size: CGSize(width: 224, height: 224)) else {
            fatalError("Unable to resize the image!")
        }
        
        if let label = self.manager.detect(resizedImage) {
            self.predictionLabel = label
        }
      
    }
    
}


extension View {
// This function changes our View to UIView, then calls another function
// to convert the newly-made UIView to a UIImage.
    public func asUIImage() -> UIImage {
        let controller = UIHostingController(rootView: self)
        
        controller.view.frame = CGRect(x: 0, y: CGFloat(Int.max), width: 1, height: 1)
        UIApplication.shared.windows.first!.rootViewController?.view.addSubview(controller.view)
        
        let size = controller.sizeThatFits(in: UIScreen.main.bounds.size)
        controller.view.bounds = CGRect(origin: .zero, size: size)
        controller.view.sizeToFit()
        
// here is the call to the function that converts UIView to UIImage: `.asImage()`
        let image = controller.view.asUIImage()
        controller.view.removeFromSuperview()
        return image
    }
}

extension UIView {
// This is the function to convert UIView to UIImage
    public func asUIImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
