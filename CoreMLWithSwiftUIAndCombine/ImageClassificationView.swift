//
//  ContentView.swift
//  CoreMLWithSwiftUIAndCombine
//
//  Created by Moritz Philip Recke on 11.03.21.
//

import SwiftUI
import CoreML

struct ImageClassificationView: View {
    
    @ObservedObject private var imageClassificationVM: ImageClassificationViewModel
    private var imageClassificationManager: ImageClassificationManager
    
    init() {
        self.imageClassificationManager = ImageClassificationManager()
        self.imageClassificationVM = ImageClassificationViewModel(manager: self.imageClassificationManager)
    }
    
    let photos = ["pineapple", "strawberry", "lemon"]
    @State private var currentIndex:Int = 0
    
    @State private var showImagePicker: Bool = false
    @State private var image: Image? = nil
    
    var body: some View {
        VStack {
            Image(photos[currentIndex])
                .resizable()
                .frame(width: 200, height: 200)
            HStack {
                Button("Back") {
                    if self.currentIndex >= self.photos.count {
                        self.currentIndex = self.currentIndex - 1
                    } else {
                        self.currentIndex   = 0
                    }
                }
                    .padding()
                    .foregroundColor(Color.white)
                    .background(Color.gray)
                Button("Next") {
                    if self.currentIndex < self.photos.count - 1 {
                        self.currentIndex = self.currentIndex + 1
                    } else {
                        self.currentIndex = 0
                    }
                }
                    .padding()
                    .foregroundColor(Color.white)
                    .background(Color.gray)
            }.padding()
            Button("Classfiy") {
                self.imageClassificationVM.detect(photos[currentIndex])
            }.padding()
            .foregroundColor(Color.white)
            .background(Color.green)
            Text(imageClassificationVM.predictionLabel)
                .padding()
                .font(.body)
            Spacer()
            image?.resizable()
                .scaledToFit()
            HStack {
                Button("Use Camera") {
                    self.showImagePicker = true
                }.padding()
                Button("Classify") {
                    self.imageClassificationVM.detect(image: image ?? Image(""))
                }.padding()
            }
        }.sheet(isPresented: self.$showImagePicker) {
            CameraView(showImagePicker: $showImagePicker, image: $image)
        }
    }
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ImageClassificationView()
            .previewDevice("iPhone 12")
    }
}
