//
//  ImageClassificationManager.swift
//  CoreMLWithSwiftUIAndCombine
//
//  Created by Moritz Philip Recke on 11.03.21.
//

import Foundation

import Foundation
import CoreML
import UIKit

class ImageClassificationManager {
    
    let model = MobileNetV2()
    
    func detect(_ img: UIImage) -> String? {
        
        guard let pixelBuffer = img.convertToBuffer(),
            let prediction = try? model.prediction(image: pixelBuffer) else {
                return nil
        }
        
        return prediction.classLabel
        
    }
    
}
